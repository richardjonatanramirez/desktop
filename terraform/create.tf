data "vsphere_datacenter" "dc" {
  name = "Datacenter - HOR"
}

data "vsphere_datastore" "datastore" {
  name          = var.data_store
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
    name          = "Resources"
    datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.mgmt_lan
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "terraform_template"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_virtual_machine" "vm" {
  name             = var.name_new_vm
  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  folder   = var.name_folder
  num_cpus = var.num_cpus
  memory   = var.num_mem
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "vmxnet3"
  }

  disk {
    label            = "disk0"
    size             = var.disk_size
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = var.name_new_vm
        domain    = "intradesk"
      }

      network_interface {
        ipv4_address = var.vsphere_ip
        ipv4_netmask = 24

      }

      ipv4_gateway = var.vsphere_gateway
	  dns_suffix_list = ["nameserver" , "intradesk"]
	  dns_server_list = ["10.0.16.40", "10.0.16.41"]
    }
  }
}