variable "data_store" {
  default = "LUN-04_Fujitsu SSD iSCSI"
}
variable "mgmt_lan" {
  default = "service_infra_ti"
}
variable "name_new_vm" {
  description = "Nome do novo host"
}
variable "vm_count" {
  default = "1"
}
variable "disk_size" {
  description = "Tamanho de disco, Ex. 50, 60, 70"
}
variable "name_folder" {
  default = "Terraform_Testes"
}
variable "num_cpus" {
  description = "Quantidade de VCPU"
}
variable "num_mem" {
  description = "Quantidade de Memoria, Ex. 1024, 2048, 3073, 4096"
}
variable "vsphere_gateway" {
  default = "172.26.12.1"
}
variable "vsphere_ip" {
  description = "Defina um ip para o host"
}
variable "vsphere_dns_server_list" {
  default = "10.0.16.40"
}