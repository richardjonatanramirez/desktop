## Template VmWare

Criado um template no Vcenter da Desktop com as seguintes configurações:
* Red Hat 8 Entreprise 64 bits;
* Instalação tipo server sem aplicativos adicionais;
* Registrado na Red Hat;
* VmTools instalada;
* usuário root com senha definida ;
* cloud-init instalado;
* Usuário dskadm criado e com a key adiocionada, além de fazer parte do grupo wheel;
* ip fixo;
* o template está localizado no folder "Terraform_Testes" no inventário do Vcenter.

## Terraform

Com informações informadas em primeiro momento, o manifesto foi criado da seguinte forma:
1. **connection**: Inicia a conexão com o Vcenter (é necessário ter comunicação com a porta 443 do Vcenter);
2. **variable**: Contém as variavéis utilizadas para criação, algumas estão definidas como default e ao iniciar o apply não irão solicitar entrada de valores, caso necessário alterar isso, basta remover esse parametro;
3. **create**: Arquivo que monta a Vm, segue os parametros utilizados:
    - **vsphere_datacenter**: definido o nome do datacenter no Vcenter onde será criado a VM;
    - **vsphere_datastore**: definido o nome do datastore utilizada, existe uma variavel default que foi usado no projeto, mas pode ser alterada para informar no momento da criação;
    - **vsphere_resource_pool**: Definido como "Resources" é o host onde a Vm será criada, nesse caso é randômico, mas pode ser definida em um Esxi especifico;
    - **vsphere_network**: Rede que a VM irá se conectar, também existe um variavel que pode ser alterada;
    - **vsphere_virtual_machine**: Template base, o mesmo foi criado no ambiente VmWare do cliente, o valor já está definido como "terraform_template", caso seja criado outro template, deve alterar esse valor;
    - **vsphere_virtual_machine**: Inicia a montagem da VM, com os recursos, rede e localização no inventário do Vcenter. OBS: Também existe uma variavel pra definir em que folder será criada a máquina, está definida default Terraform_Testes e pode ser alterada;
    - **adapter_type**: Padrão vmxnet3;
    - **domain**: Padrão intradesk;
    - **dns**: Estão como padrão, caso mude é necessário alterar ip e names em **dns_suffix_list** e **dns_server_list**.

## Criando uma VM com Terraform

Edite o arquivo [connection](terraform/connection.tf) com as credenciais e execute:
````
terraform init
````
Em seguida:
````
terraform plan ##Caso queira planejar
````
````
terraform apply ##Em caso de execução direta
````
Quando o deploy iniciar será questionado os valores das variaveis não setadas como default

## Configurando a VM com Ansible

Criado um playbook que instala e configura alguns requisitos da Desktop:
- Instala e configura o Zabbix;
- Libera e bloqueia portas no iptables;
- Bloqueia o root de realizar login via ssh.

## Executando o playbook

Esse playbook foi criado para executar suas tarefas apartir do usuário dskadm com senha, para isso é necessario alterar seu ansible.cfg para aceitar esse tipo de execução:
````
vi /etc/ansible/ansible.cfg
````
Em seguida adicione a seguinte linha:
````
[defaults]
host_key_checking = False
````
Agora adicione o ip da VM a ser configurada no seu /etc/ansible/hosts ou crie um arquivo hosts novo somente para essa execução.
Execute o playbook:
````
ansible-playbook config.yml -kK  ###Caso de edição em /etc/ansible/hosts
````
ou
````
ansible-playbook -i hosts config.yml -kK ##Informe o arquivo hosts
````
Por fim digite a senha do usuário dskadm e aguarde a execução das tarefas.



## Referências
- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html
- https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs
- https://www.zabbix.com/documentation/4.4/en/manual/installation/install_from_packages/rhel_centos
- https://github.com/gersontpc/ansible-zabbix-agent
- https://developer.vmware.com/apis/358/doc/vim.vm.GuestOsDescriptor.GuestOsIdentifier.html




